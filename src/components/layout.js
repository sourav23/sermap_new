import * as React from "react"
import { Link } from "gatsby"
import Header from "./common/Header"
import Footer from "./common/Footer"
import "./layout.css"

const Layout = ({ location, title, children }) => {
  // const rootPath = `${__PATH_PREFIX__}/`
  // const isRootPath = location.pathname === rootPath
  // let header

  // if (isRootPath) {
  //   header = (
  //     <h1 className="main-heading">
  //       <Link to="/">{title}</Link>
  //     </h1>
  //   )
  // } else {
  //   header = (
  //     <Link className="header-link-home" to="/">
  //       {title}
  //     </Link>
  //   )
  // }

  // data-is-root-path={isRootPath}

  return (
    <div className="global-wrapper" >
      {/* <header className="global-header">
      {header}
      </header> */}
      <Header/>
      
      <main>{children}</main>
      <Footer/>
      {/* <footer>
        © {new Date().getFullYear()}, Built with
        {` `}
        <a href="https://www.gatsbyjs.com">Gatsby</a>
      </footer> */}
    </div>
  )
}

export default Layout

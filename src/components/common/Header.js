/* eslint-disable jsx-a11y/accessible-emoji */
import React, { useState, useEffect } from "react";
import {
    withStyles, Box, Typography,
    Select,
    MenuItem, FormControl,
    InputLabel,
    Button,
    Grid,
    RadioGroup,
    FormControlLabel,
    Radio,
    CircularProgress,
    FormGroup,
    Checkbox,
    Container,
    Icon,
    Chip,
    Link
} from '@mui/material'
import MdPhone from '@mui/icons-material/Phone';
import LogoutIcon from '@mui/icons-material/Logout';

import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import DraftsIcon from '@mui/icons-material/Drafts';
import SendIcon from '@mui/icons-material/Send';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Logo from '../../images/logo.png'
import { axios } from "../../axios";

function Header({ classes }) {

    const [open, setOpen] = useState(false);

    // useEffect(()=>{
    //     getList()
    // },[])

    // const getList= async ()=>{
    //     const response= await axios.get("/MasterApi/school_list");
    //     console.log("MasterApiSchoolList",response.data.data)
    // }

    const handleClick = () => {
        setOpen(!open);
    };
    return (
        <div style={{ width: "100%", height: "15vh", backgroundColor: "white" }}>
            {/* <Container> */}
            <Grid container>
                <Grid item lg={6} md={6}>
                    <Box style={{ width: "100%", height: "6vh", backgroundColor: "#000", color: "#fff" }} paddingLeft={2} paddingY={2}>
                        <Typography>504A, PS AVIATOR, Chinar Park, Rajarhat</Typography>
                    </Box>
                </Grid>
                <Grid item lg={6} md={6}>
                    <Box style={{ width: "100%", height: "6vh", backgroundColor: "#000", justifyContent: "end" }} display="flex">
                        <Box style={{ textAlign: "center", }} paddingX={3} paddingY={1}>
                            <Chip icon={<MdPhone style={{ color: "#fff" }} />} style={{ color: "#fff" }} label="Customer Care Services" />
                        </Box>
                        <Box style={{ textAlign: "center", color: "#fff", width: "17%" }}>
                            <List
                            >
                                <ListItemButton onClick={handleClick} style={{ marginTop: "-12px" }}>
                                    <ListItemText primary="Register" style={{ fontSize: "12px" }} />
                                </ListItemButton>
                                <Collapse in={open} timeout="auto" unmountOnExit>
                                    <List component="div" disablePadding style={{ backgroundColor: "#fff" }}>
                                        <ListItemButton >
                                            <ListItemText primary="Student" style={{ color: "#000" }} />
                                        </ListItemButton>
                                        <ListItemButton >
                                            <ListItemText primary="Teacher" style={{ color: "#000" }} />
                                        </ListItemButton>
                                        <ListItemButton >
                                            <ListItemText primary="Game Academy" style={{ color: "#000" }} />
                                        </ListItemButton>
                                    </List>
                                </Collapse>
                            </List>

                        </Box>
                        <Box style={{ textAlign: "center" }} paddingX={2} paddingY={1}>
                            <Chip icon={<LogoutIcon style={{ color: "#fff" }} />} style={{ color: "#fff" }} label="Sign In" />
                        </Box>

                    </Box>
                </Grid>
            </Grid>

            <Grid container spacing={1}>
                <Grid item lg={6} md={6}>
                    <Box style={{ height: "10vh" }}>
                        <img src={Logo} alt="" style={{ width: "258px" }} />
                    </Box>

                </Grid>
                <Grid item lg={6} md={6}>
                    <Box display="flex" style={{ height: "10vh" }}>

                        <Box style={{ height: "10vh", width: "20%", textAlign: "center" }} paddingY={3}>
                            <Link href="/">
                                <Typography >Home</Typography>
                            </Link>
                        </Box>


                        <Box style={{ height: "10vh", width: "20%", textAlign: "center" }} paddingY={3}>
                            <Link href="/about_us">
                                <Typography>About Us</Typography>
                            </Link>
                        </Box>

                        <Box style={{ height: "10vh", width: "20%", textAlign: "center" }} paddingY={3}>
                            <Link href="/services">
                                <Typography>Services</Typography>
                            </Link>
                        </Box >
                        <Box style={{ height: "10vh", width: "20%", textAlign: "center" }} paddingY={3}>
                            <Link href="/teachers">
                                <Typography>Teachers</Typography>
                            </Link>
                        </Box>
                        <Box style={{ height: "10vh", width: "20%", textAlign: "center" }} paddingY={3}>
                            <Link href="/contact_us">
                                <Typography>Contact Us</Typography>
                            </Link>
                        </Box>
                    </Box>
                </Grid>

            </Grid>
            {/* </Container> */}
            {/* <div style={{ width: "50%" }}>
                
            </div>
            <div style={{ backgroundColor: "red", width: "50%", display:"flex", flexDirection:"row" }}>
                <Typography>Home</Typography>
                <Typography>About</Typography>
                <Typography>Teacher</Typography>
                <Typography>Student</Typography>
            </div> */}



        </div>
    );
}
// const styles = (theme) => ({
//     root:{

//     },
//     navItem:{
//         height: "100vh",
//     width: "20%"
//     }
// });
export default Header

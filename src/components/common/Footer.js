/* eslint-disable jsx-a11y/accessible-emoji */
import { Container, Grid, Box, Typography, Chip, Divider } from "@mui/material";
import React, { useState, useEffect } from "react";
import MdPhone from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import MapIcon from '@mui/icons-material/Map';
import FacebookIcon from '@mui/icons-material/Facebook';
import GoogleIcon from '@mui/icons-material/Google';
import TwitterIcon from '@mui/icons-material/Twitter';
import LinkedinIcon from '@mui/icons-material/LinkedIn';

import Logo from '../../images/logo.png'

function Footer() {


    return (
        <div style={{ width: "100%", backgroundColor: "#000" }}>
            <Container style={{ height: "45vh" }}>
                <Grid container>
                    <Grid item lg={12} md={12}>
                        <Box style={{ height: "10vh", width: "100%", textAlign: "center" }} marginTop={5}>
                            <img src={Logo} alt="" style={{ width: "258px", zIndex: "999" }} />
                        </Box>
                    </Grid>
                </Grid>

                <Grid container>
                    <Grid item lg={4}>
                        <Box display="flex" flexDirection="column">
                            <Box>
                                <Typography style={{ color: "#fff" }}>ABOUT SER-MAPS</Typography>
                            </Box>
                            <Box></Box>
                            <Typography style={{ color: "#fff" }}>Lorem Ipsum again its nothing but just a simple text , which takes place intead of real data . It means its again proved that its dummy.</Typography>
                            <Box>
                                <Chip icon={<MdPhone style={{ color: "#fff" }} />} style={{ color: "#fff" }} label="123 125 4242" />
                            </Box>
                            <Box>
                                <Chip icon={<EmailIcon style={{ color: "#fff" }} />} style={{ color: "#fff" }} label="contact@sermaps.com" />
                            </Box>
                            <Box>
                                <Chip icon={<MapIcon style={{ color: "#fff" }} />} style={{ color: "#fff" }} label="504A, PS AVIATOR, Chinar Park, Rajarhat" />
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item lg={4}>
                        <Box display="flex" flexDirection="column" style={{ textAlign: "center" }}>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Useful LInks</Typography>
                            </Box>
                            <Box></Box>
                            <Typography style={{ color: "#fff" }}>Popular Classes</Typography>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Popular Videos</Typography>
                            </Box>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Latest Courses</Typography>
                            </Box>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Notifications</Typography>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item lg={4}>
                        <Box display="flex" flexDirection="column" style={{ textAlign: "center" }}>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Latest Videos</Typography>
                            </Box>
                            <Box></Box>
                            <Typography style={{ color: "#fff" }}>Videos</Typography>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Videos</Typography>
                            </Box>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Videos</Typography>
                            </Box>
                            <Box>
                                <Typography style={{ color: "#fff" }}>Videos</Typography>
                            </Box>
                        </Box>
                    </Grid>
                </Grid>
                <Divider/>
                <Grid container>
                    <Grid item lg={6} md={6}>
                        <Box style={{ height: "10vh", width: "100%", textAlign: "left" }} marginTop={5}>
                           <Typography>© Copyright 2021 , sermaps.com All Rights Reserved</Typography>
                        </Box>
                    </Grid>
                    <Grid item lg={6} md={6}>
                        <Box style={{ height: "10vh", width: "100%", textAlign: "right" }} marginTop={5}>
                            <FacebookIcon style={{marginRight:"4px"}}/>

                            <TwitterIcon style={{marginRight:"4px"}}/>
                            <GoogleIcon style={{marginRight:"4px"}}/>
                            <LinkedinIcon/>
                        </Box>
                    </Grid>
                </Grid>

            </Container>
        </div>
    );
}
export default Footer
